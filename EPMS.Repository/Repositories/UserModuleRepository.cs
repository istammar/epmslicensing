﻿using System.Data.Entity;
using System.Linq;
using EPMS.Interfaces.Repository;
using EPMS.Repository.BaseRepository;
using EPMS.Models.DomainModels;
using Microsoft.Practices.Unity;

namespace EPMS.Repository.Repositories
{
    public class UserModuleRepository : BaseRepository<UserModule>, IUserModuleRepository
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UserModuleRepository(IUnityContainer container)
            : base(container)
        {
        }

        /// <summary>
        /// Primary database set
        /// </summary>
        protected override IDbSet<UserModule> DbSet
        {
            get { return db.UserModules; }
        }

        #endregion

        public UserModule FindUserModuleByName(string name)
        {
            return DbSet.FirstOrDefault(module => module.ModuleName == name);
        }
    }
}
