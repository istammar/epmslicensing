﻿using System.Data.Entity;
using EPMS.Interfaces.Repository;
using EPMS.Repository.BaseRepository;
using EPMS.Repository.Repositories;
using Microsoft.Practices.Unity;

namespace EPMS.Repository
{
    public static class TypeRegistrations
    {
        public static void RegisterType(IUnityContainer unityContainer)
        {
            unityContainer.RegisterType<IAspNetUserRepository, AspNetUserRepository>();
            unityContainer.RegisterType<DbContext, BaseDbContext>(new PerRequestLifetimeManager());
            unityContainer.RegisterType<ILicenseControlPanelRepository, LicenseControlPanelRepository>();
            unityContainer.RegisterType<IUserModuleRepository, UserModuleRepository>();
            unityContainer.RegisterType<IHelpRepository, HelpRepository>();
        }
    }
}
