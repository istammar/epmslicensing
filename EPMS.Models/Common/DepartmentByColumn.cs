﻿namespace EPMS.Models.Common
{
    public enum DepartmentByColumn
    {
        DepartmentId = 1,
        DepartmentName = 2,
        DepartmentDesc = 3
    }
}
