﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPMS.Models.Common
{
    public enum PayrollByColumn
    {
        EmployeeId = 1,
        EmployeeNameE = 2,
        EmployeeJobId = 3
    }
}
