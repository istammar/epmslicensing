﻿namespace EPMS.Models.Common
{
    /// <summary>
    /// Job Applicant Sort Columns
    /// </summary>
    public enum JobApplicantByColumn
    {
        ApplicantName = 1,
        ApplicantEmail = 2,
        ApplicantMobile = 3,
        JobOffered = 4,
        Departemnt = 5
    }
}
