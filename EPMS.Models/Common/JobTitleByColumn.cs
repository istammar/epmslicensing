﻿namespace EPMS.Models.Common
{
    public enum JobTitleByColumn
    {
        JobTitleId = 1,
        JobTitleName = 2,
        JobTitleDesc = 3,
        BasicSalary = 4
    }
}
