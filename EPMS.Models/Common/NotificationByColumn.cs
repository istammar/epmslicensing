﻿namespace EPMS.Models.Common
{
    public enum NotificationByColumn
    {
        SerialNo = 0,
        TitleE=1,
        CategoryId=2,
        AlertBefore=3,
        AlertDate=4,
        EmployeeName=5,
        MobileNo=6,
        Email=7,
        ReadStatus=8
    }
}
