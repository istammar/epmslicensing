﻿namespace EPMS.Models.Common
{
    public enum EmployeeByColumn
    {
        EmployeeNameE = 2,
        EmployeeJobId = 3,
        EmployeeJobTitle = 4,
        EmployeeDepartment = 5
    }
}
