﻿namespace EPMS.Models.Common
{
    public enum OrdersByColumn
    {
        OrderNumber = 1,
        ClientName = 2,
        Quotation = 3,
        Invoice = 4,
        Reciepts = 5,
        Status = 6,
    }
}
