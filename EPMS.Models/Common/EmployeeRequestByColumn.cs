﻿namespace EPMS.Models.Common
{
    public enum EmployeeRequestByColumn
    {
        RequestTopic = 1,
        EmployeeName = 2,
        JobId = 3,
        Department = 4,
        Date = 5,
        Status = 6,
        Replied=7
    }
}
