﻿namespace EPMS.Models.Common
{
    /// <summary>
    /// Meeting Sort Columns
    /// </summary>
    public enum MeetingByColumn
    {
        Topic = 1,
        TopicAr = 2,
        RelatedProject = 3,
        Date = 4,
    }
}
