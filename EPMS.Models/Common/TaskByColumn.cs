﻿namespace EPMS.Models.Common
{
    public enum TaskByColumn
    {
        TaskName = 1,
        ProjectName = 2,
        StartDate = 3,
        DeliveryDate = 4,
        Cost = 5,
        Progress = 6
    }
}
