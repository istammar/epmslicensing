﻿using System.Collections.Generic;

namespace EPMS.Models.DomainModels
{
    public class Help
    {
        public int HelpNodeId { get; set; }
        public string TitleEn { get; set; }
        public string TitleAr { get; set; }
        public string MenuType { get; set; }
        public string ContentEn { get; set; }
        public string ContentAr { get; set; }
        public int? ParentNode_HelpNodeId { get; set; }

        public virtual ICollection<Help> HelpItems { get; set; }
        public virtual Help HelpParent { get; set; }
    }
}
