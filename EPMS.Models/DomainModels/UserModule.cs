﻿namespace EPMS.Models.DomainModels
{
    public class UserModule
    {
        public long UserModuleId { get; set; }
        public long LicenseControlPanelId { get; set; }
        public string ModuleName { get; set; }

        public virtual LicenseControlPanel LicenseControlPanel { get; set; }
    }
}
