﻿namespace EPMS.Models.ResponseModels
{
    public class AvailableOrdersDDL
    {
        public long OrderId { get; set; }
        public string OrderNo { get; set; }
    }
}
