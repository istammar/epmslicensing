﻿using EPMS.Models.DomainModels;

namespace EPMS.Interfaces.Repository
{
    public interface IHelpRepository : IBaseRepository<Help, int>
    {
    }
}
