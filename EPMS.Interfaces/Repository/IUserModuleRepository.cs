﻿using EPMS.Models.DomainModels;

namespace EPMS.Interfaces.Repository
{
    public interface IUserModuleRepository : IBaseRepository<UserModule, int>
    {
        UserModule FindUserModuleByName(string name);
    }
}
