﻿using EPMS.Models.DomainModels;

namespace EPMS.Interfaces.Repository
{
    public interface ILicenseControlPanelRepository : IBaseRepository<LicenseControlPanel, int>
    {

    }
}
