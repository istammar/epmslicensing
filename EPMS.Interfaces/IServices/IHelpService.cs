﻿using System.Collections.Generic;
using EPMS.Models.DomainModels;

namespace EPMS.Interfaces.IServices
{
    public interface IHelpService
    {
        Help FindHelpItemById(int? id);
        IEnumerable<Help> GetAll();
        bool AddHelpItem(Help help);
        bool UpdateHelpItem(Help help);
        void DeleteHelpItem(Help help);
    }
}
