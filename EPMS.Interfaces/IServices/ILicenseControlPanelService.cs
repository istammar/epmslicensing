﻿using System.Collections.Generic;
using EPMS.Models.DomainModels;

namespace EPMS.Interfaces.IServices
{
    public interface ILicenseControlPanelService
    {
        LicenseControlPanel FindLicenseById(int? id);
        IEnumerable<LicenseControlPanel> GetAll();
        bool AddLicense(LicenseControlPanel licenseControlPanel, IList<string> selectedModules);
        bool UpdateLicense(LicenseControlPanel licenseControlPanel, IList<string> selectedModules, IList<string> dbModules);
        void DeleteLicense(LicenseControlPanel licenseControlPanel);
    }
}
