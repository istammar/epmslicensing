﻿using System.Collections.Generic;
using EPMS.Models.DomainModels;

namespace EPMS.Interfaces.IServices
{
    public interface IUserModuleService
    {
        UserModule FindUserModuleById(int? id);
        UserModule FindUserModuleByName(string name);
        IEnumerable<UserModule> GetAll();
        bool AddUserModule(UserModule userModule);
        bool UpdateUserModule(UserModule userModule);
        void DeleteUserModule(UserModule userModule);
    }
}
