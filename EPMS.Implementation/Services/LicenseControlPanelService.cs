﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPMS.Interfaces.IServices;
using EPMS.Interfaces.Repository;
using EPMS.Models.DomainModels;

namespace EPMS.Implementation.Services
{
    public class LicenseControlPanelService : ILicenseControlPanelService
    {
        private readonly ILicenseControlPanelRepository Repository;
        private readonly IUserModuleService moduleService;
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LicenseControlPanelService(ILicenseControlPanelRepository repository, IUserModuleService moduleService)
        {
            Repository = repository;
            this.moduleService = moduleService;
        }

        #endregion

        public LicenseControlPanel FindLicenseById(int? id)
        {
            return Repository.Find(Convert.ToInt32(id));
        }

        public IEnumerable<LicenseControlPanel> GetAll()
        {
            return Repository.GetAll();
        }

        public bool AddLicense(LicenseControlPanel licenseControlPanel, IList<string> selectedModules)
        {
            try
            {
                Repository.Add(licenseControlPanel);
                Repository.SaveChanges();
                IList<bool> isAdded = new List<bool>();
                foreach (var selectedModule in selectedModules)
                {
                    UserModule userModule = new UserModule { ModuleName = selectedModule, LicenseControlPanelId = licenseControlPanel.LicenseControlPanelId };
                    if (moduleService.AddUserModule(userModule))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateLicense(LicenseControlPanel licenseControlPanel, IList<string> selectedModules, IList<string> dbModules)
        {
            try
            {
                Repository.Update(licenseControlPanel);
                Repository.SaveChanges();
                // Check selected in dbList
                #region Add Module
                List<string> newModuleToAdd = new List<string>();
                // Compare new to db pre-tasks to Add new
                foreach (var module in selectedModules)
                {
                    if (!dbModules.Contains(module))
                    {
                        newModuleToAdd.Add(module);
                    }
                }
                if (newModuleToAdd.Any())
                {
                    foreach (string module in newModuleToAdd)
                    {
                        UserModule userModule = new UserModule { ModuleName = module, LicenseControlPanelId = licenseControlPanel.LicenseControlPanelId };
                        if (moduleService.AddUserModule(userModule))
                        {
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                #endregion

                #region Remove Pre-Req Tasks
                List<string> moduleListToRemove = new List<string>();
                // Compare db pre-tasks to new to Remove
                foreach (var module in dbModules)
                {
                    if (!selectedModules.Contains(module))
                    {
                        moduleListToRemove.Add(module);
                    }
                }
                if (moduleListToRemove.Any())
                {
                    foreach (string module in moduleListToRemove)
                    {
                        var moduleToRemove = moduleService.FindUserModuleByName(module);
                        moduleService.DeleteUserModule(moduleToRemove);
                    }
                }
                #endregion
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteLicense(LicenseControlPanel licenseControlPanel)
        {
            Repository.Delete(licenseControlPanel);
            Repository.SaveChanges();
        }
    }
}
