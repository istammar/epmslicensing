﻿using System;
using System.Collections.Generic;
using EPMS.Interfaces.IServices;
using EPMS.Interfaces.Repository;
using EPMS.Models.DomainModels;

namespace EPMS.Implementation.Services
{
    public class HelpService : IHelpService
    {
        private readonly IHelpRepository Repository;
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public HelpService(IHelpRepository repository)
        {
            Repository = repository;
        }
        #endregion

        public Help FindHelpItemById(int? id)
        {
            if (id != null) return Repository.Find((int)id);
            return null;
        }

        public IEnumerable<Help> GetAll()
        {
            return Repository.GetAll();
        }

        public bool AddHelpItem(Help help)
        {
            try
            {
                Repository.Add(help);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateHelpItem(Help help)
        {
            try
            {
                Repository.Update(help);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteHelpItem(Help help)
        {
            Repository.Delete(help);
            Repository.SaveChanges();
        }
    }
}
