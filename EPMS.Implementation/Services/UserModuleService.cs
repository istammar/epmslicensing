﻿using System;
using System.Collections.Generic;
using EPMS.Interfaces.IServices;
using EPMS.Interfaces.Repository;
using EPMS.Models.DomainModels;

namespace EPMS.Implementation.Services
{
    public class UserModuleService : IUserModuleService
    {
        private readonly IUserModuleRepository Repository;
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public UserModuleService(IUserModuleRepository repository)
        {
            Repository = repository;
        }

        #endregion
        public UserModule FindUserModuleById(int? id)
        {
            return Repository.Find(Convert.ToInt32(id));
        }

        public UserModule FindUserModuleByName(string name)
        {
            return Repository.FindUserModuleByName(name);
        }

        public IEnumerable<UserModule> GetAll()
        {
            return Repository.GetAll();
        }

        public bool AddUserModule(UserModule userModule)
        {
            try
            {
                Repository.Add(userModule);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateUserModule(UserModule userModule)
        {
            try
            {
                Repository.Update(userModule);
                Repository.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteUserModule(UserModule userModule)
        {
            Repository.Delete(userModule);
            Repository.SaveChanges();
        }
    }
}
