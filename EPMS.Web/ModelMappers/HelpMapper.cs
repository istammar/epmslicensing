﻿using System.Linq;
using EPMS.Models.DomainModels;

namespace EPMS.Web.ModelMappers
{
    public static class HelpMapper
    {
        public static Models.Help CreateFromServerToClient(this Help source)
        {
            var retVal = new Models.Help();
            retVal.HelpNodeId = source.HelpNodeId;
            retVal.TitleEn = source.TitleEn;
            retVal.TitleAr = source.TitleAr;
            retVal.ContentEn = source.ContentEn;
            retVal.ContentAr = source.ContentAr;
            retVal.MenuType = source.MenuType;
            retVal.ParentNode_HelpNodeId = source.ParentNode_HelpNodeId;
            return retVal;
        }
        public static Models.Help CreateFromServerToClientForNodes(this Help source)
        {
            var retVal = new Models.Help();
            retVal.HelpNodeId = source.HelpNodeId;
            retVal.TitleEn = source.TitleEn;
            retVal.TitleAr = source.TitleAr;
            retVal.MenuType = source.MenuType;
            retVal.ParentNode_HelpNodeId = source.ParentNode_HelpNodeId;
            return retVal;
        }
        public static Help CreateFromClientToServer(this Models.Help source)
        {
            var retVal = new Help();
            retVal.HelpNodeId = source.HelpNodeId;
            retVal.TitleEn = source.TitleEn;
            retVal.TitleAr = source.TitleAr;
            retVal.ContentEn = source.ContentEn;
            retVal.ContentAr = source.ContentAr;
            retVal.MenuType = source.MenuType;
            retVal.ParentNode_HelpNodeId = source.ParentNode_HelpNodeId;
            return retVal;
        }
    }
}