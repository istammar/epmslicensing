﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPMS.Models.DomainModels;

namespace EPMS.Web.ModelMappers
{
    public static class UserModuleMapper
    {
        public static UserModule CreateFromClientToServer(this Models.UserModule source)
        {
            return new UserModule
            {
                LicenseControlPanelId = source.LicenseControlPanelId,
                UserModuleId = source.UserModuleId,
                ModuleName = source.ModuleName
            };
        }
        public static Models.UserModule CreateFromServerToClient(this UserModule source)
        {
            return new Models.UserModule
            {
                LicenseControlPanelId = source.LicenseControlPanelId,
                ModuleName = source.ModuleName,
                UserModuleId = source.UserModuleId
            };
        }
    }
}