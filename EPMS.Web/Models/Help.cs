﻿using System.Collections.Generic;

namespace EPMS.Web.Models
{
    public class Help
    {
        public int HelpNodeId { get; set; }
        public string TitleEn { get; set; }
        public string TitleAr { get; set; }
        public string MenuType { get; set; }
        public string ContentEn { get; set; }
        public string ContentAr { get; set; }
        public int? ParentNode_HelpNodeId { get; set; }
    }
}