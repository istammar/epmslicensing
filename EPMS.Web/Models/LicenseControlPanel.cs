﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EPMS.Web.Models
{
    public class LicenseControlPanel
    {
        public LicenseControlPanel()
        {
            UserModules = new List<UserModule>();
        }
        public long LicenseControlPanelId { get; set; }
        [Required(ErrorMessage = "Company Name is required")]
        public string CompanyName { get; set; }
        public string Website { get; set; }
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }
        public string Address { get; set; }
        public string LandLine { get; set; }
        public string Mobile { get; set; }
        public string CommercialRegister { get; set; }
        public string ProductNumber { get; set; }
        [Required(ErrorMessage = "No of Users Required")]
        public int NoOfUsers { get; set; }
        public string LicenseNumber { get; set; }
        [Required(ErrorMessage = "Start date is required")]
        public string StartDate { get; set; }
        [Required(ErrorMessage = "End date is required")]
        public string EndDate { get; set; }
        public bool Status { get; set; }
        [Required(ErrorMessage = "Domain is required")]
        public string Domain { get; set; }
        public string MacAddress { get; set; }
        public string LicenseKey { get; set; }
        public IList<UserModule> UserModules { get; set; }
    }
}