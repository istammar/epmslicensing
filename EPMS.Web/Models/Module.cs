﻿namespace EPMS.Web.Models
{
    public class Module
    {
        public string Val { get; set; }
        public string Text { get; set; }
    }
}