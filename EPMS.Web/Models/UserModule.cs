﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPMS.Web.Models
{
    public class UserModule
    {
        public long UserModuleId { get; set; }
        public long LicenseControlPanelId { get; set; }
        public string ModuleName { get; set; }
    }
}