﻿using System.Collections.Generic;

namespace EPMS.Web.ViewModels.Help
{
    public class HelpIndexViewModel
    {
        public HelpIndexViewModel()
        {
            Help = new List<Models.Help>();
            HelpItem = new Models.Help();
        }
        public IList<Models.Help> Help { get; set; }
        public Models.Help HelpItem { get; set; }
    }
}