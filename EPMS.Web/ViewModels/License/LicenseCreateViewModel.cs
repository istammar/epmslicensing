﻿using System.Collections.Generic;
using EPMS.Web.Models;

namespace EPMS.Web.ViewModels.License
{
    public class LicenseCreateViewModel
    {
        public LicenseCreateViewModel()
        {
            ModulesList = new List<Module>();
            OldSelectedModules = new List<string>();
            LicenseCoP = new LicenseControlPanel();
        }
        public LicenseControlPanel LicenseCoP { get; set; }
        public IList<Module> ModulesList { get; set; }
        public IList<string> SelectedModules { get; set; }
        public IList<string> OldSelectedModules { get; set; }
    }
}