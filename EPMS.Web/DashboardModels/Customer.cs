﻿namespace EPMS.Web.DashboardModels
{
    public class Customer
    {
        public long CustomerId { get; set; }
        public string CustomerNameE { get; set; }
        public string CustomerNameA { get; set; }
    }
}