﻿namespace EPMS.Web.DashboardModels
{
    public class Recruitment
    {
        public long JobOfferedId { get; set; }
        public string TitleE { get; set; }
        public string TitleA { get; set; }
        public int NoOfApplicants { get; set; }
    }
}