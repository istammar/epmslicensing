﻿namespace EPMS.Web.DashboardModels
{
    public class Meeting
    {
        public long MeetingId { get; set; }
        public string Topic { get; set; }
        public string TopicA { get; set; }
        public string TopicShort { get; set; }
        public string TopicAShort { get; set; }
        public string MeetingDate { get; set; }
    }
}