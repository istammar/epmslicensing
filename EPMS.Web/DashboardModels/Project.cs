﻿namespace EPMS.Web.DashboardModels
{
    public class Project
    {
        public long ProjectId { get; set; }
        public string NameE { get; set; }
        public string NameA { get; set; }
    }
}