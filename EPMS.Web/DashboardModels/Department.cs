﻿namespace EPMS.Web.DashboardModels
{
    public class Department
    {
        public long DepartmentId { get; set; }
        public string DepartmentNameE { get; set; }
        public string DepartmentNameA { get; set; }
    }
}