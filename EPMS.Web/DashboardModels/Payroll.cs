﻿namespace EPMS.Web.DashboardModels
{
    public class Payroll
    {
        public double BasicSalary { get; set; }
        public double Allowances { get; set; }
        public double Deductions { get; set; }
        public double Total { get; set; }
    }
}