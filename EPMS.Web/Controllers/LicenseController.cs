﻿using System;
using System.Linq;
using System.Web.Mvc;
using EPMS.Interfaces.IServices;
using EPMS.Web.ModelMappers;
using EPMS.Web.Models;
using EPMS.Web.ViewModels.Common;
using EPMS.Web.ViewModels.License;

namespace EPMS.Web.Controllers
{
    [Authorize]
    public class LicenseController : BaseController
    {
        #region Private
        private readonly ILicenseControlPanelService ControlPanelService;
        private readonly IUserModuleService ModuleService;
        
        #endregion

        #region Constructor
        public LicenseController(ILicenseControlPanelService controlPanelService, IUserModuleService moduleService)
        {
            ControlPanelService = controlPanelService;
            ModuleService = moduleService;
        }

        #endregion

        // GET: LCP/License
        public ActionResult Index()
        {
            LicenseIndexViewModel viewModel = new LicenseIndexViewModel
            {
                LicenseControlPanels = ControlPanelService.GetAll().Select(x => x.CreateFromServerToClient())
            };
            ViewBag.MessageVM = TempData["message"] as MessageViewModel;
            return View(viewModel);
        }
        // GET: LCP/Create
        public ActionResult Create(int? id)
        {
            LicenseCreateViewModel viewModel = new LicenseCreateViewModel();
            viewModel.ModulesList.Add(new Module
            {
                Val = "PMS",
                Text = "Project Management System"
            });
            //viewModel.ModulesList.Add(new Module
            //{
            //    Val = "HRS",
            //    Text = "Human Resource System"
            //});
            viewModel.ModulesList.Add(new Module
            {
                Val = "Mt",
                Text = "Meetings"
            });
            viewModel.ModulesList.Add(new Module
            {
                Val = "CS",
                Text = "Customer Service"
            });
            viewModel.ModulesList.Add(new Module
            {
                Val = "Dashboard",
                Text = "Dashboard"
            });
            //viewModel.ModulesList.Add(new Module
            //{
            //    Val = "NS",
            //    Text = "Notification System"
            //});
            if (id != null)
            {
                viewModel.LicenseCoP = ControlPanelService.FindLicenseById(Convert.ToInt32(id)).CreateFromServerToClient();
                foreach (var modules in viewModel.LicenseCoP.UserModules)
                {
                    viewModel.OldSelectedModules.Add(modules.ModuleName);
                }
            }
            return View(viewModel);
        }
        /// <summary>
        /// POST: LCP/Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(LicenseCreateViewModel viewModel)
        {
            if (Request.Form["Deactivate"] != null)
            {
                viewModel.LicenseCoP.Status = false;
                var licenseToUpdate = viewModel.LicenseCoP.CreateFromClientToServer();
                if (ControlPanelService.UpdateLicense(licenseToUpdate, viewModel.SelectedModules, viewModel.OldSelectedModules))
                {
                    TempData["message"] = new MessageViewModel
                    {
                        Message = "License has been Updated",
                        IsUpdated = true
                    };
                    return RedirectToAction("Index");
                }
            }
            if (Request.Form["Activate"] != null)
            {
                // Generate License
                string domain = viewModel.LicenseCoP.Domain;
                long noOfUsers = viewModel.LicenseCoP.NoOfUsers;
                string expiryDate = viewModel.LicenseCoP.EndDate;
                string modules = "";
                foreach (var module in viewModel.SelectedModules)
                {
                    modules = modules + module + ";";
                }
                modules = modules + "HRS;NS;CL;CP;HI";
                var productNo = viewModel.LicenseCoP.ProductNumber;
                var licenseNo = viewModel.LicenseCoP.LicenseNumber;
                var startDate = viewModel.LicenseCoP.StartDate;
                string licenseKey = domain + "|" + viewModel.LicenseCoP.MacAddress + "|" + Convert.ToString(noOfUsers) + "|" +
                                    expiryDate + "|" + modules + "|" + productNo + "|" + licenseNo + "|" + startDate;
                // (string to encrypt, encryption password )
                string encryptedString = EncryptDecrypt.EncryptDecrypt.StringCipher.Encrypt(licenseKey, "123");
                const string emailSubject = "License Key for EPMS";
                string emailBody = "Your License has been generated. License key is = " + encryptedString;
                Utility.SendEmailAsync(viewModel.LicenseCoP.Email, emailSubject, emailBody);
                if (viewModel.LicenseCoP.LicenseControlPanelId == 0)
                {
                    viewModel.LicenseCoP.Status = true;
                    viewModel.LicenseCoP.LicenseKey = encryptedString;
                    var licenseToAdd = viewModel.LicenseCoP.CreateFromClientToServer();
                    if (ControlPanelService.AddLicense(licenseToAdd, viewModel.SelectedModules))
                    {
                        TempData["message"] = new MessageViewModel
                        {
                            Message = "License has been Added",
                            IsSaved = true
                        };
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    viewModel.LicenseCoP.Status = true;
                    viewModel.LicenseCoP.LicenseKey = encryptedString;
                    var licenseToUpdate = viewModel.LicenseCoP.CreateFromClientToServer();
                    if (ControlPanelService.UpdateLicense(licenseToUpdate, viewModel.SelectedModules, viewModel.OldSelectedModules))
                    {
                        TempData["message"] = new MessageViewModel
                        {
                            Message = "License has been Updated",
                            IsUpdated = true
                        };
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(viewModel);
        }
    }
}