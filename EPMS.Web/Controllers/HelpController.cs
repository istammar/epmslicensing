﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using EPMS.Interfaces.IServices;
using EPMS.Web.ModelMappers;
using EPMS.Web.Models;
using EPMS.Web.ViewModels.Help;
using Newtonsoft.Json;

namespace EPMS.Web.Controllers
{
    [AllowAnonymous]
    public class HelpController : BaseController
    {
        #region Private
        private readonly IHelpService HelpService;
        
        #endregion

        #region Constructor
        public HelpController(IHelpService helpService)
        {
            HelpService = helpService;
        }

        #endregion
        // GET: Help
        public ActionResult Index()
        {
            var isAuthenticated = User.Identity.IsAuthenticated;
            if (!isAuthenticated)
            {
                return RedirectToAction("Help");
            }
            HelpIndexViewModel viewModel = new HelpIndexViewModel();
            var helpItems = HelpService.GetAll();
            viewModel.Help = helpItems.Select(x=>x.CreateFromServerToClient()).ToList();
            return View(viewModel);
        }

        public ActionResult Help()
        {
            HelpIndexViewModel viewModel = new HelpIndexViewModel();
            var helpItems = HelpService.GetAll();
            viewModel.Help = helpItems.Select(x=>x.CreateFromServerToClient()).ToList();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveHelpNode(int HelpNodeId, string Parent, string TitleEn, string TitleAr, string MenuType, string ContentEn, string ContentAr)
        {
            HelpIndexViewModel viewModel = new HelpIndexViewModel();
            viewModel.HelpItem.TitleEn = TitleEn;
            viewModel.HelpItem.TitleAr = TitleAr;
            viewModel.HelpItem.MenuType = MenuType;
            if (MenuType == "Content")
            {
                var content = ContentEn.Replace("\n", "");
                var contentAr = ContentAr.Replace("\n", "");
                content = content.Replace("\"", "'");
                content = content.Replace("\t", "");
                contentAr = contentAr.Replace("\"", "'");
                contentAr = contentAr.Replace("\t", "");
                viewModel.HelpItem.ContentEn = content;
                viewModel.HelpItem.ContentAr = contentAr;
            }
            else
            {
                viewModel.HelpItem.ContentEn = "";
                viewModel.HelpItem.ContentAr = "";
            }
            if (Parent == "node1")
            {
                viewModel.HelpItem.ParentNode_HelpNodeId = null;
            }
            else
            {
                viewModel.HelpItem.ParentNode_HelpNodeId = Convert.ToInt32(Parent);
            }
            if (HelpNodeId == 0)
            {
                var newNodeToAdd = viewModel.HelpItem.CreateFromClientToServer();
                if (HelpService.AddHelpItem(newNodeToAdd))
                {
                    var helpItems = HelpService.GetAll();
                    viewModel.Help = helpItems.Select(x => x.CreateFromServerToClient()).ToList();
                    return Json(viewModel, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                viewModel.HelpItem.HelpNodeId = HelpNodeId;
                var newNodeToUpdate = viewModel.HelpItem.CreateFromClientToServer();
                if (HelpService.UpdateHelpItem(newNodeToUpdate))
                {
                    var helpItems = HelpService.GetAll();
                    viewModel.Help = helpItems.Select(x => x.CreateFromServerToClient()).ToList();
                    return Json(viewModel, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}