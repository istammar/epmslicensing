﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EPMS.Web.Controllers
{
    public class CultureController : Controller
    {
        public ActionResult Set(string id)
        {
            CultureInfo info;
            if (id == "AR")
            {
                info = new CultureInfo("ar");
                System.Threading.Thread.CurrentThread.CurrentUICulture = info;
                System.Threading.Thread.CurrentThread.CurrentCulture = info;
                Session["Culture"] = "ar";
            }
            else
            {
                info = new CultureInfo("en");
                System.Threading.Thread.CurrentThread.CurrentUICulture = info;
                System.Threading.Thread.CurrentThread.CurrentCulture = info;
                Session["Culture"] = "en";
            }
            string redirectUrl = Request.UrlReferrer.ToString();
            if (string.IsNullOrEmpty(redirectUrl))
            {
                return RedirectToAction("Index", "License");
            }
            return Redirect(redirectUrl);
        }
    }
}